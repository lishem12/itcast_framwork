package com.lishem.server;

public class Main {

    // 开启netty服务线程
    public static void main(String[] args) throws Exception {
        new NettyProviderServer(9911).runServer();
    }
}
