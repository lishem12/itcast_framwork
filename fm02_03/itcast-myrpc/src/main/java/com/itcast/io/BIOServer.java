package com.itcast.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/***
 * @author:shenkunlin
 * @description:
 * @date:2021/3/30 4:17
 ***/
public class BIOServer {

    public static void main(String[] args) throws IOException {
        //创佳ServerSocket
        ServerSocket serverSocket = new ServerSocket(6666);
        //创建线程池
        ExecutorService executorService = Executors.newCachedThreadPool();

        //循环处理链接对象
        while (true){
            System.out.println("阻塞等待客户端链接！");
            Socket socket = serverSocket.accept();

            //读取数据
            executorService.execute(()->{
                try {
                    //获取数据流
                    InputStream is = socket.getInputStream();
                    //定义缓冲区
                    byte[] buffer = new byte[1024];
                    //循环读取数据
                    int len = 0;
                    while ((len=is.read(buffer))!=-1){
                        System.out.println(new String(buffer,0,len,"UTF-8"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
