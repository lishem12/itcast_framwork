package com.itcast.io;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

/***
 * @author:shenkunlin
 * @description:
 * @date:2021/3/30 4:32
 ***/
public class NIOServer {

    /***
     * 向Selector注册Channel
     */
    public Selector getSelector() throws Exception{
        //创建Selector
        Selector selector = Selector.open();

        //创建Channel
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(false);//非阻塞
        
        //创建Socket，并绑定指定端口
        ServerSocket socket = serverSocketChannel.socket();
        socket.bind(new InetSocketAddress(6677));

        //向Selector注册Channel,事件为连接准备就绪事件
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        return selector;
    }


    /***
     * 循环处理每个Channel事件
     */
    public void listen() throws Exception {
        Selector selector = getSelector();
        //循环处理每个Channel
        while (true){
            //该方法会阻塞，直到至少有一个准备就绪的事件发生
            selector.select();
            //获取所有准备就绪的事件
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            while (iterator.hasNext()){
                //获取准备就绪的事件对象
                SelectionKey selectionKey = iterator.next();
                //处理对应的事件信息
                process(selectionKey,selector);
                //从Selector中移除该Channel
                iterator.remove();
            }
        }
    }


    /***
     * 数据处理
     */
    public void process(SelectionKey key, Selector selector) throws IOException {
        //判断事件,并做相关处理
        if(key.isAcceptable()){
            //连接准备就绪事件
            ServerSocketChannel server = (ServerSocketChannel) key.channel();
            SocketChannel channel = server.accept();
            //向Selector注册读事件
            channel.configureBlocking(false);
            channel.register(selector,SelectionKey.OP_READ);
        }else if(key.isReadable()){
            //读准备就绪事件
            SocketChannel channel = (SocketChannel) key.channel();
            //创建ByteBuffer并且设置最大大小为1024
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            //将Channel中的数据写入到ByteBuffer中
            int len = channel.read(byteBuffer);
            while (len != -1) {
                //重置读的位置
                byteBuffer.flip();
                while(byteBuffer.hasRemaining()){
                    //读数据
                    System.out.print((char) byteBuffer.get());
                }
                //重置ByteBuffer指针数据，准备写入数据到ByteBuffer中
                byteBuffer.clear();
                len = channel.read(byteBuffer);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        new NIOServer().listen();
    }
}
