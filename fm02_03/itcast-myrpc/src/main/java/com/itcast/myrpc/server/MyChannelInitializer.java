package com.itcast.myrpc.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

/***
 * @author:shenkunlin
 * @description:
 * @date:2021/3/30 21:35
 ***/
public class MyChannelInitializer extends ChannelInitializer<SocketChannel> {

    /***
     * 多个业务链添加
     * @param ch
     * @throws Exception
     */
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new MyChannelHandler());
    }
}
