package com.itcast.bytebuftest;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.buffer.UnpooledByteBufAllocator;
import io.netty.channel.ChannelOption;
import io.netty.util.CharsetUtil;

/**
 * ByteBuf清理功能
 */
public class ByteBufClear {

    public static void main(String[] args) {
        
        ByteBuf byteBuf = Unpooled.copiedBuffer("hello world", CharsetUtil.UTF_8);
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); // 64
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); //11
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); //53
        byteBuf.clear(); //重置readerIndex 、 writerIndex 为0
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); //64
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); //0
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); // 64
    }
}
