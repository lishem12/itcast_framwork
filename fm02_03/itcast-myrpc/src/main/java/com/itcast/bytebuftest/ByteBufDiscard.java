package com.itcast.bytebuftest;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.util.CharsetUtil;

/**
 * 丢弃测试
 */
public class ByteBufDiscard {

    public static void main(String[] args) {
        ByteBuf byteBuf = Unpooled.copiedBuffer("hello world", CharsetUtil.UTF_8);
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); // 64
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); // 11
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); // 53
        while (byteBuf.isReadable()) {
            System.out.println((char) byteBuf.readByte());
        }

        byteBuf.discardReadBytes(); //丢弃已读的字节空间
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); // 64
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); // 0
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); // 64

    }
}
