package com.itcast.bytebuftest;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

/**
 * ByteBuf写测试
 */
public class ByteBufWrite {

    public static void main(String[] args) {
        //构造空的字节缓冲区，初始大小为10，最大为20
        ByteBuf byteBuf = Unpooled.buffer(10, 20);
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); // 10
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); // 0
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); // 10
        for (int i = 0; i < 5; i++) {
            byteBuf.writeInt(i); //写入int类型，一个int占4个字节
        }
        System.out.println("ok");
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); // 20
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); // 20
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); // 0
        while (byteBuf.isReadable()) {
            System.out.println(byteBuf.readInt());
        }
    }
}
