package com.itcast.test;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.util.CharsetUtil;
import org.junit.Test;

/***
 * @author:shenkunlin
 * @description:
 * @date:2021/3/30 22:21
 ***/
public class ByteBufTest {

    /***
     * ByteBuf写数据
     */
    @Test
    public void testWrite(){
        //构造空的字节缓冲区，初始大小为10，最大为20
        ByteBuf byteBuf = Unpooled.buffer(10,20);

        System.out.println("byteBuf的容量为：" + byteBuf.capacity());
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes());
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes());

        for (int i = 0; i < 5; i++) {
            byteBuf.writeInt(i); //写入int类型，一个int占4个字节
        }
        System.out.println("ok");

        System.out.println("byteBuf的容量为：" + byteBuf.capacity());
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes());
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes());

        while (byteBuf.isReadable()){
            System.out.println(byteBuf.readInt());
        }
    }

    /***
     * 读测试
     */
    @Test
    public void testRead(){
        //构造
        ByteBuf byteBuf = Unpooled.copiedBuffer("hello world", CharsetUtil.UTF_8);

        System.out.println("byteBuf的容量为：" + byteBuf.capacity());
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes());
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes());

        //方法一：内部通过移动readerIndex进行读取
        while (byteBuf.isReadable()){
            System.out.println((char)byteBuf.readByte());
        }

        //方法二：通过下标直接读取
        /*for (int i = 0; i < byteBuf.readableBytes(); i++) {
            System.out.println((char)byteBuf.getByte(i));
        }*/

        //方法三：转化为byte[]进行读取
        /*byte[] bytes = byteBuf.array();
        for (byte b : bytes) {
            System.out.println((char)b);
        }*/
    }
}
