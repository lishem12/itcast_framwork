package com.itcast.test;

import com.itcast.myrpc.client.MyClientServer;
import com.itcast.myrpc.server.MyRPCServer;
import org.junit.Test;

/***
 * @author:shenkunlin
 * @description:
 * @date:2021/3/30 21:29
 ***/
public class RPCServerTest {
    /***
     * Netty服务端启动
     */
    @Test
    public void testMyRPCServerStart(){
        System.setProperty("io.netty.noUnsafe", "true");
        MyRPCServer rpcServer = new MyRPCServer();
        rpcServer.start(5566);
    }

    /****
     * Netty客户端
     */
    @Test
    public void testMyClientServerStart(){
        MyClientServer myClientServer = new MyClientServer();
        myClientServer.start("127.0.0.1",5566);
    }
}
