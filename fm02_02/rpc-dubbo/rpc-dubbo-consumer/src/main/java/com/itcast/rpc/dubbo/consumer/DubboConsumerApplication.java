package com.itcast.rpc.dubbo.consumer;

import com.itcst.rpc.dubbo.api.service.OrderService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
public class DubboConsumerApplication {

    @Reference
    private OrderService orderService;

    public static void main(String[] args) {
        SpringApplication.run(DubboConsumerApplication.class, args);
    }

    /**
     * 下单接口
     * localhost:18080/placeOrder?username=wangwu
     * @param userName
     * @return
     */
    @GetMapping("/placeOrder")
    public String placeOrder(String userName) {
        try{
            return orderService.placeOrder(userName == null ? "Dubbo": userName);
        }catch(Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
