package com.itcst.rpc.dubbo.api.service;
/**
 * <p>Description: </p>
 * @date 
 * @author 
 * @version 1.0
 * <p>Copyright:Copyright(c)2020</p>
 */
public interface OrderService {
    /**
     * 下单接口
     * @param userName
     * @return
     */
    String placeOrder(String userName) throws Exception;
}
