package com.itcast.rpc.dubbo.provider.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.AbstractRule;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.itcst.rpc.dubbo.api.service.OrderService;
import lombok.extern.log4j.Log4j2;
import org.apache.dubbo.config.annotation.Service;

/**
 * <p>Description: </p>
 * @date 
 * @author 
 * @version 1.0
 * <p>Copyright:Copyright(c)2020</p>
 */
@Service
@Log4j2
public class OrderServiceImpl implements OrderService {



    /**
     * 下单限流处理逻辑
     * @param userNo
     * @param userPwd
     * @param ex
     * @return
     * @throws BlockException
     */
    public String placeOrderHanlder(String userName, BlockException ex) throws Exception {
        // 1. 获取熔断限流规则
        AbstractRule abstractRule = ex.getRule();
        // 2. 根据不同规则， 进行不同逻辑处理
        System.out.println(abstractRule);
        log.info(abstractRule);
        if(abstractRule instanceof DegradeRule) {
            return "进入降级规则！";
        }else if(abstractRule instanceof FlowRule) {
            return "进入限流规则！";
        }
        return "熔断限流处理！";
    }


    /**
     * 下单接口
     * @param userName
     * @return
     */
    @Override
    @SentinelResource(value ="placeOrder", blockHandler = "placeOrderHanlder")
    public String placeOrder(String userName) throws Exception{
        if ("error".equals(userName)) {
            throw new Exception("occur error!");
        }
        return userName + "下单成功！";
    }
}
