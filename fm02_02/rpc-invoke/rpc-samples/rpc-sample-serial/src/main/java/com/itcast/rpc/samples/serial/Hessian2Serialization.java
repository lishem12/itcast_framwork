package com.itcast.rpc.samples.serial;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import com.itcast.rpc.samples.serial.user.TradeUser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * <p>Description: </p>
 * @date 
 * @author 
 * @version 1.0
 * <p>Copyright:Copyright(c)2020</p>
 */
public class Hessian2Serialization {

    public static void main(String[] args) throws Exception {
        serialize();
    }

    public static void serialize() throws Exception {
        TradeUser tradeUser = new TradeUser();
        tradeUser.setName("Mirson");
        tradeUser.setUserNo("100001");

        //tradeUser对象序列化处理
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Hessian2Output output = new Hessian2Output(bos);
        output.writeObject(tradeUser);
        output.flushBuffer();
        byte[] data = bos.toByteArray();
        System.out.println("=== 序列化结果 ==== ");
        System.out.println(data);
        bos.close();

        //tradeUser对象反序列化处理
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        Hessian2Input input = new Hessian2Input(bis);
        TradeUser deTradeUser = (TradeUser) input.readObject();
        input.close();
        System.out.println("=== 反序列化结果 ==== ");
        System.out.println(deTradeUser);

    }
}
