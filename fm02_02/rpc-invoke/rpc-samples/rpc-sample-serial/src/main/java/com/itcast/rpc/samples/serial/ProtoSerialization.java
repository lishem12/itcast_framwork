package com.itcast.rpc.samples.serial;

import com.itcast.rpc.samples.serial.proto.TradeUserProto;

/**
 * <p>Description: </p>
 * @date 
 * @author 
 * @version 1.0
 * <p>Copyright:Copyright(c)2020</p>
 */
public class ProtoSerialization {

    public static void serialize() throws Exception{

        // 创建TradeUser的Protobuf对象
        TradeUserProto.TradeUser.Builder builder = TradeUserProto.TradeUser.newBuilder();
        builder.setUserId(100001);
        builder.setUserName("Mirson");

        //将TradeUser做序列化处理
        TradeUserProto.TradeUser msg = builder.build();
        byte[] data = msg.toByteArray();
        System.out.println("=== 序列化结果 ==== ");
        System.out.println(data);

        //反序列化处理, 将刚才序列化的byte数组转化为TradeUser对象
        TradeUserProto.TradeUser deTradeUser = TradeUserProto.TradeUser.parseFrom(data);
        System.out.println("=== 反序列化结果 ==== ");
        System.out.println(deTradeUser);

    }
}
