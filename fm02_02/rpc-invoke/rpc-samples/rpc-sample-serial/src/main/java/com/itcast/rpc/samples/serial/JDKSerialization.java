package com.itcast.rpc.samples.serial;

import com.itcast.rpc.samples.serial.user.TradeUser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * <p>Description: </p>
 * @date 
 * @author 
 * @version 1.0
 * <p>Copyright:Copyright(c)2020</p>
 */
public class JDKSerialization {

    public static void serialize() throws Exception {
        //将序列化后的数据存入到D:/TestCode/tradeUser.clazz中
        String basePath =  "D:/TestCode/";
        FileOutputStream fos = new FileOutputStream(basePath + "tradeUser.clazz");
        //创建tradeUser对象
        TradeUser tradeUser = new TradeUser();
        tradeUser.setName("Mirson");
        //将tradeUser写入到tradeUser.clazz中
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(tradeUser);
        oos.flush();
        oos.close();

        //读取D:/TestCode/tradeUser.clazz
        FileInputStream fis = new FileInputStream(basePath + "tradeUser.clazz");
        ObjectInputStream ois = new ObjectInputStream(fis);
        //将读取的数据反序列化到对象中
        TradeUser deTradeUser = (TradeUser) ois.readObject();
        ois.close();
        System.out.println("=== 反序列化结果 ==== ");
        System.out.println(deTradeUser);
    }

    public static void main(String[] args) throws Exception {
        serialize();
    }
}
