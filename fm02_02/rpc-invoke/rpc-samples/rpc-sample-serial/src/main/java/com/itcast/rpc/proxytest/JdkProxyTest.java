package com.itcast.rpc.proxytest;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class JdkProxyTest {

    /**
     * 定义用户的接口
     */
    public interface User {
        String job();
    }

    public static class Teacher{
        public String invoke(){
            return "im a teacher";
        }
    }

    /**
     * 创建JDK动态代理类
     */
    public static class JDKProxy implements InvocationHandler{

        private Object target;

        public JDKProxy(Object target){
            this.target = target;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            System.out.println("代理前插入");
            Object invoke = method.invoke(proxy, args);
            System.out.println("代理后插入");
            return invoke;
        }
    }

    public static void main(String[] args) {
        JDKProxy proxy = new JDKProxy(new Teacher());

    }

}
