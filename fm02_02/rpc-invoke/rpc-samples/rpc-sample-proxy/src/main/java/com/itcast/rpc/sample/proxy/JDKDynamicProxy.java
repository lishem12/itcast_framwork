package com.itcast.rpc.sample.proxy;

import sun.misc.ClassLoaderUtil;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * <p>Description: </p>
 * @date 
 * @author 
 * @version 1.0
 * <p>Copyright:Copyright(c)2020</p>
 */
public class JDKDynamicProxy {

    /**
     * 定义用户的接口
     */
    public interface User {
        String job();
    }


    /**
     * 实际的调用对象
     */
    public static class Teacher  {

        public String invoke(){
            return "i'm a Teacher";
        }
    }


    /**
     * 创建JDK动态代理类
     */
    public static class JDKProxy implements InvocationHandler {
        private Object target;

        JDKProxy(Object target) {
            this.target = target;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] paramValues) {
            return ((Teacher)target).invoke();
        }
    }


    public static void main(String[] args){
        // 构建代理器
        JDKProxy proxy = new JDKProxy(new Teacher());
        ClassLoader classLoader = JDKDynamicProxy.class.getClassLoader();

        // 生成代理类
        User user = (User) Proxy.newProxyInstance(classLoader, new Class[]{User.class}, proxy);

        // 接口调用
        System.out.println(user.job());
    }
}
