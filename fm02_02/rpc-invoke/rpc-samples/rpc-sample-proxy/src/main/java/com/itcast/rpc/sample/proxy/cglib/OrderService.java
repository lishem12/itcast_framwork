package com.itcast.rpc.sample.proxy.cglib;
/**
 * <p>Description: </p>
 * @date 
 * @author 
 * @version 1.0
 * <p>Copyright:Copyright(c)2020</p>
 */
public class OrderService {

    /**
     * 下单接口
     */
    public String placeOrder(String userName) {
        System.out.println("用户：" + userName + "开始下单");
        return "成功";
    }
}
