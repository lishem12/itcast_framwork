package com.itcast.rpc.sample.proxy;

import com.itcast.rpc.sample.proxy.cglib.OrderService;
import com.itcast.rpc.sample.proxy.cglib.OrderServiceCglib;

/**
 * <p>Description: </p>
 * @date 
 * @author 
 * @version 1.0
 * <p>Copyright:Copyright(c)2020</p>
 */
public class RpcProxyApplication {

    public static void main(String[] args) {
        // Cglib 动态代理
        cglibTest();
    }

    /**
     * cglib 演示
     */
    public static void cglibTest() {
        // 1. 创建Cglib动态代理
        OrderServiceCglib cglib = new OrderServiceCglib();
        // 2. 配置实现类
        OrderService orderService = (OrderService) cglib.getProxy(new OrderService());
        // 3. 通过动态代理调用接口
        orderService.placeOrder("mirson");
    }
}
