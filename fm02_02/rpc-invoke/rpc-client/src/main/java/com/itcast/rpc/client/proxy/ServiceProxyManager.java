package com.itcast.rpc.client.proxy;

import com.itcast.common.annotation.RpcClient;
import com.itcast.common.utils.SpringBeanFactory;
import com.itcast.rpc.client.config.RpcClientConfiguration;
import org.apache.commons.collections.CollectionUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * 动态代理管理器
 */
@Component
public class ServiceProxyManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceProxyManager.class);

    @Autowired
    private RpcClientConfiguration configuration;

    @Autowired
    private ProxyHelper proxyHelper;

    public void initServiceProxyInstance() {
        Reflections reflections = new Reflections(configuration.getRpcClientApiPackage());
        Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(RpcClient.class);
        if (CollectionUtils.isEmpty(typesAnnotatedWith)) {
            return;
        }

        DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) SpringBeanFactory.context()
                .getAutowireCapableBeanFactory();
        for (Class<?> cls : typesAnnotatedWith) {
            RpcClient znsClient = cls.getAnnotation(RpcClient.class);
            String serviceName = cls.getName();
            beanFactory.registerSingleton(serviceName, proxyHelper.newProxyInstance(cls));
        }

        LOGGER.info("Initialize proxy for service successfully");
    }
}
