package com.itcast.rpc.client.cluster;


import com.itcast.rpc.client.channel.ProviderService;

import java.util.List;

/**
 * 集群调用策略接口
 */
public interface ClusterStrategy {

    ProviderService select(List<ProviderService> serviceRoutes);
}
