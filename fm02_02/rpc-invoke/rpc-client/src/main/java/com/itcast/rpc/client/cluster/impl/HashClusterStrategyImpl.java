package com.itcast.rpc.client.cluster.impl;

import com.itcast.common.utils.IpUtil;
import com.itcast.rpc.client.channel.ProviderService;
import com.itcast.rpc.client.cluster.ClusterStrategy;

import java.util.List;

/**
 * HASH策略
 */
public class HashClusterStrategyImpl implements ClusterStrategy {

    @Override
    public ProviderService select(List<ProviderService> serviceRoutes) {
        String realIp = IpUtil.getRealIp();
        int hashCode = realIp.hashCode();

        int size = serviceRoutes.size();
        return serviceRoutes.get(hashCode % size);
    }
}
