package com.itcast.rpc.client.startup;

import com.itcast.rpc.client.cache.ServiceRouteCache;
import com.itcast.rpc.client.zk.ServicePullManager;
import com.itcast.rpc.client.proxy.ServiceProxyManager;
import com.itcast.rpc.client.runner.RpcRequestManager;
import com.itcast.rpc.client.runner.RpcRequestPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Rpc客户端启动实现
 */
@Component
public class RpcClientRunner {

    @Autowired
    private ServicePullManager servicePullManager;

    @Autowired
    private ServiceProxyManager serviceProxyManager;

    @Autowired
    private RpcRequestPool rpcRequestPool;

    @Autowired
    private ServiceRouteCache serviceRouteCache;

    public void run() {
        // Start request manager
        RpcRequestManager.startRpcRequestManager(rpcRequestPool, serviceRouteCache);

        // Pull service provider info from zookeeper
        servicePullManager.pullServiceFromZK();

        // Create proxy for service which owns @ZnsClient annotation
        serviceProxyManager.initServiceProxyInstance();
    }
}
