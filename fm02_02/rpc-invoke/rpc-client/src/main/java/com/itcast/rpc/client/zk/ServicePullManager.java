package com.itcast.rpc.client.zk;

import com.itcast.common.annotation.RpcClient;
import com.itcast.rpc.client.cache.ServiceRouteCache;
import com.itcast.rpc.client.channel.ProviderService;
import com.itcast.rpc.client.config.RpcClientConfiguration;
import org.apache.commons.collections.CollectionUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * 注册服务拉取管理器
 */
@Component
public class ServicePullManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServicePullManager.class);

    @Autowired
    private ZKit zKit;

    @Autowired
    private ServiceRouteCache serviceRouteCache;

    @Autowired
    private RpcClientConfiguration configuration;

    public void pullServiceFromZK() {
        Reflections reflections = new Reflections(configuration.getRpcClientApiPackage());
        Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(RpcClient.class);
        if (CollectionUtils.isEmpty(typesAnnotatedWith)) {
            return;
        }
        for (Class<?> cls : typesAnnotatedWith) {
            String serviceName = cls.getName();

            // Cache service provider list into local
            List<ProviderService> providerServices = zKit.getServiceInfos(serviceName);
            serviceRouteCache.addCache(serviceName, providerServices);

            // Add listener for service node
            zKit.subscribeZKEvent(serviceName);
        }

        LOGGER.info("Pull service address list from zookeeper successfully");
    }
}
