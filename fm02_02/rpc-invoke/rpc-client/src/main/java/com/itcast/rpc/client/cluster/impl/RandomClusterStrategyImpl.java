package com.itcast.rpc.client.cluster.impl;

import com.itcast.rpc.client.channel.ProviderService;
import com.itcast.rpc.client.cluster.ClusterStrategy;
import org.apache.commons.lang3.RandomUtils;

import java.util.List;

/**
 * 随机策略
 */
public class RandomClusterStrategyImpl implements ClusterStrategy {

    @Override
    public ProviderService select(List<ProviderService> serviceRoutes) {
        int MAX_LEN = serviceRoutes.size();
        int index = RandomUtils.nextInt(0, MAX_LEN - 1);
        return serviceRoutes.get(index);
    }
}
