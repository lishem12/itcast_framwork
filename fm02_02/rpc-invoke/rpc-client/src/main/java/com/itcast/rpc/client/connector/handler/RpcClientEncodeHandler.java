package com.itcast.rpc.client.connector.handler;

import com.itcast.common.data.RpcRequest;
import com.itcast.common.utils.JsonSerializerUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * Rpc客户端编码器
 */
public class RpcClientEncodeHandler extends MessageToByteEncoder<RpcRequest> {

    @Override
    protected void encode(ChannelHandlerContext ctx, RpcRequest RpcRequest, ByteBuf in) throws Exception {
        byte[] bytes = JsonSerializerUtil.serialize(RpcRequest);
        in.writeInt(bytes.length);
        in.writeBytes(bytes);
    }
}
