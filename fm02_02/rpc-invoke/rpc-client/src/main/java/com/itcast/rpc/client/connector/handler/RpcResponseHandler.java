package com.itcast.rpc.client.connector.handler;

import com.itcast.common.data.RpcResponse;
import com.itcast.rpc.client.runner.RpcRequestPool;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Rpc数据接收响应处理器
 */
@Component
@ChannelHandler.Sharable
public class RpcResponseHandler extends SimpleChannelInboundHandler<RpcResponse> {

    @Autowired
    private RpcRequestPool requestPool;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcResponse znsResponse) throws Exception {
        requestPool.notifyRequest(znsResponse.getRequestId(), znsResponse);
    }
}
