package com.lishem.server.connector.handler;

import com.lishem.common.data.RpcRequest;
import com.lishem.common.data.RpcResponse;
import com.lishem.common.utils.SpringBeanFactory;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Rpc的服务端数据接收处理
 */
@Component
@Slf4j
@ChannelHandler.Sharable
public class RpcRequestHandler extends SimpleChannelInboundHandler<RpcRequest> {

    /**
     * 数据接收处理
     * @param ctx
     * @param request
     * @throws Exception
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcRequest request) throws Exception {
        //3、封装响应对象->RpcResponse
        RpcResponse response = new RpcResponse();

        //1、解析传过来的数据RpcRequest->ClassName->com.lishem...->Method->parameter..
        String requestId = request.getRequestId();
        String className = request.getClassName();
        String methodName = request.getMethodName();
        Class<?>[] parameterTypes = request.getParameterTypes();
        Object[] parameters = request.getParameters();

        try {
            //2、找到Class对应的方法执行反射调用
            Object targetClass = SpringBeanFactory.getBean(Class.forName(className));
            Method targetMethod = targetClass.getClass().getMethod(methodName, parameterTypes);
            Object result = targetMethod.invoke(targetClass, parameters);

            //3、封装响应对象->RpcResponse
            response.setRequestId(requestId);
            response.setResult(result);
        } catch (Throwable e) {
            response.setCause(e);
        }
        //4、响应结果
        ctx.writeAndFlush(response);
    }
}
