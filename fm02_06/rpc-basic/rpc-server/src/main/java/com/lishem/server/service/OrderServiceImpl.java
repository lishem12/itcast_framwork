package com.lishem.server.service;

import com.lishem.common.annotation.RpcService;
import com.lishem.api.OrderService;
import com.lishem.common.utils.RpcException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RpcService(cls = OrderService.class) // 扫描的这个注解
public class OrderServiceImpl implements OrderService {

    @Value("${server.port}")
    private Integer serverPort;

    @Override
    public String getOrder(String userName, String orderNo) {
        if ("error".equalsIgnoreCase(userName)) {
            throw new RpcException("test exception! " + userName);
        }
        return String.format("Server(" + serverPort + "), Order Details => userName: %s, orderNo: %s", userName, orderNo);
    }
}
