package com.lishem.client.connector;

import com.lishem.client.channel.ChannelHolder;
import com.lishem.client.channel.ProviderService;
import com.lishem.client.runner.RpcRequestManager;
import com.lishem.client.runner.RpcRequestPool;
import com.lishem.common.utils.SpringBeanFactory;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;

/**
 * Rpc客户端连接器(异步操作，多线程启动)
 *
 * 以前的建立链接都是在发送消息的时候，每次发送都重新建立连接
 * 现在把建立连接抽取出来
 */
@Slf4j
public class RpcClientConnector implements Runnable {

    private String requestId;
    private ProviderService providerService;
    private CountDownLatch latch;
    private RpcClientInitializer znsClientInitializer;

    public RpcClientConnector(String requestId, ProviderService providerService, CountDownLatch latch) {
        this.requestId = requestId;
        this.providerService = providerService;
        this.latch = latch;
        this.znsClientInitializer = SpringBeanFactory.getBean(RpcClientInitializer.class);
    }

    @Override
    public void run() {
        // 1. 创建Netty链接配置
        EventLoopGroup worker = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(worker)
                .channel(NioSocketChannel.class)
                .remoteAddress(providerService.getServerIp(), providerService.getNetworkPort())
                .handler(znsClientInitializer);

        try {
            // 2. 建立连接
            ChannelFuture future = bootstrap.connect().sync();
            if (future.isSuccess()) {
                // 封装当前链接对应的Channel对象和 EventLoopGroup 对象
                ChannelHolder channelHolder = ChannelHolder.builder()
                        .channel(future.channel())
                        .eventLoopGroup(worker)
                        .build();

                // 将对应的请求（requestId）的ChannelHolder的对象传递到发送请求对象中
                // 这个操作送了封装返回结果到 RpcRequestPool 中
                RpcRequestManager.registerChannelHolder(requestId, channelHolder);
                log.info("Construct a connector with service provider[{}:{}] successfully",
                        providerService.getServerIp(),
                        providerService.getNetworkPort()
                );

                // 倒计数 值-1
                latch.countDown();
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
