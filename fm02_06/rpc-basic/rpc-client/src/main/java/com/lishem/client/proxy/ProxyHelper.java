package com.lishem.client.proxy;

import com.lishem.client.runner.RpcRequestManager;
import com.lishem.client.runner.RpcRequestPool;
import com.lishem.common.data.RpcRequest;
import com.lishem.common.data.RpcResponse;
import com.lishem.common.utils.RequestIdUtil;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 动态代理拦截处理器
 */
@Component
public class ProxyHelper {

    // 消息处理对象
    @Autowired
    private RpcRequestPool requestPool;

    /**
     * 创建新的代理实例-CGLib动态代理
     *
     * @param cls
     * @param <T>
     * @return
     */
    public <T> T newProxyInstance(Class<T> cls) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(cls);
        enhancer.setCallback(new ProxyCallBackHandler());
        return (T) enhancer.create();
    }

    /**
     * 代理拦截实现
     */
    class ProxyCallBackHandler implements MethodInterceptor {

        @Override
        public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
            //return doIntercept(method, args);
            return doInterceptAsync(method, args);
        }


        /**
         * 拦截RCP接口调用 异步处理
         *
         * @param method
         * @param parameters
         * @return
         * @throws Throwable
         */
        private Object doInterceptAsync(Method method, Object[] parameters) throws Throwable {
            //1、封装RpcRequest请求参数
            String requestId = RequestIdUtil.requestId(); //唯一会话ID
            String methodName = method.getName();
            String className = method.getDeclaringClass().getName();
            Class<?>[] parameterTypes = method.getParameterTypes();

            RpcRequest request = RpcRequest.builder()
                    .requestId(requestId)
                    .className(className)
                    .methodName(methodName)
                    .parameterTypes(parameterTypes)
                    .parameters(parameters)
                    .build();

            // 向服务端发消息
            RpcRequestManager.sendRequestAsync(request);
            RpcResponse rpcResponse = requestPool.fetchResponse(requestId);

            //3、获取返回结果
            return rpcResponse.getResult();
        }

        /**
         * 拦截RCP接口调用 同步处理
         *
         * @param method
         * @param parameters
         * @return
         * @throws Throwable
         */
        private Object doIntercept(Method method, Object[] parameters) throws Throwable {
            //1、封装RpcRequest请求参数
            String requestId = RequestIdUtil.requestId(); //唯一会话ID
            String methodName = method.getName();
            String className = method.getDeclaringClass().getName();
            Class<?>[] parameterTypes = method.getParameterTypes();

            RpcRequest request = RpcRequest.builder()
                    .requestId(requestId)
                    .className(className)
                    .methodName(methodName)
                    .parameterTypes(parameterTypes)
                    .parameters(parameters)
                    .build();

            //2、发送数据
            RpcResponse response = RpcRequestManager.sendRequest(request);

            //3、获取返回结果
            return response.getResult();
        }
    }
}
