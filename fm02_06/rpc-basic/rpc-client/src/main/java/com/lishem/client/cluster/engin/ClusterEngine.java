package com.lishem.client.cluster.engin;

import com.google.common.collect.Maps;
import com.lishem.client.cluster.ClusterStrategy;
import com.lishem.client.cluster.ClusterStrategyEnum;
import com.lishem.client.cluster.impl.HashClusterStrategyImpl;
import com.lishem.client.cluster.impl.PollingClusterStrategyImpl;
import com.lishem.client.cluster.impl.RandomClusterStrategyImpl;
import com.lishem.client.cluster.impl.WeightRandomClusterStrategyImpl;

import java.util.Map;

/**
 * 集群调动策略处理引擎
 */
public class ClusterEngine {

    private static final Map<ClusterStrategyEnum, ClusterStrategy> clusterStrategyMap = Maps.newConcurrentMap();

    /**
     * 定义所有的负载策略
     */
    static {
        clusterStrategyMap.put(ClusterStrategyEnum.RANDOM, new RandomClusterStrategyImpl());
        clusterStrategyMap.put(ClusterStrategyEnum.WEIGHT_RANDOM, new WeightRandomClusterStrategyImpl());
        clusterStrategyMap.put(ClusterStrategyEnum.POLLING, new PollingClusterStrategyImpl());
        clusterStrategyMap.put(ClusterStrategyEnum.HASH, new HashClusterStrategyImpl());
    }

    /**
     * 根据策略选取指定的实现引擎
     * @param clusterStrategy
     * @return
     */
    public static ClusterStrategy queryClusterStrategy(String clusterStrategy) {
        ClusterStrategyEnum clusterStrategyEnum = ClusterStrategyEnum.queryByCode(clusterStrategy);
        if (clusterStrategyEnum == null) {
            // 默认才去随机策略
            clusterStrategyEnum = ClusterStrategyEnum.RANDOM;
        }
        return clusterStrategyMap.get(clusterStrategyEnum);
    }
}
