package com.lishem.client.runner;

import com.lishem.common.data.RpcResponse;

import java.util.concurrent.Future;

public interface RequestFuture<T> extends Future<T> {

    Throwable cause();

    void setCause(Throwable cause);

    boolean isWriteSuccess();

    void setWriteResult(boolean result);

    String requestId();

    T response();

    void setResponse(RpcResponse response);

    boolean isTimeout();


}