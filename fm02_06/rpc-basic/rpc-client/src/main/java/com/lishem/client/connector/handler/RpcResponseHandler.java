package com.lishem.client.connector.handler;

import com.lishem.client.runner.RequestFuture;
import com.lishem.client.runner.RpcRequestPool;
import com.lishem.client.runner.SyncRequestFuture;
import com.lishem.common.data.RpcResponse;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Rpc数据接收响应处理器
 */
@Component
@ChannelHandler.Sharable
@Slf4j
public class RpcResponseHandler extends SimpleChannelInboundHandler<RpcResponse> {

    @Autowired
    private RpcRequestPool requestPool;

    // 异步调用的
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcResponse msg) throws Exception {
        // 将数据填充到对应请求的Future中
        requestPool.notifyRequest(msg.getRequestId(),msg);

    }

    // 同步调用的
//    @Override
//    protected void channelRead0(ChannelHandlerContext ctx, RpcResponse znsResponse) throws Exception {
//        // 获取请求回调信息
//        RequestFuture requestFuture = SyncRequestFuture.syncRequest.get(znsResponse.getRequestId());
//        if(null != requestFuture) {
//            // 设置回调结果
//            requestFuture.setResponse(znsResponse);
//        }else {
//            log.warn("未能找到回调信息！" + znsResponse);
//        }
//    }


}

