package com.lishem.client.cluster.impl;

import com.lishem.client.channel.ProviderService;
import com.lishem.client.cluster.ClusterStrategy;
import com.lishem.common.utils.IpUtil;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用客户端ip做Hash，然后确定调用哪个服务
 * 某客户端会永远调用同一个服务
 */
@Component("hash")
public class HashClusterStrategyImpl implements ClusterStrategy {

    /**
     * serviceRoutes 服务集合列表
     *
     * @param serviceRoutes
     * @return ProviderService 返回要确定的服务地址
     */
    @Override
    public ProviderService select(List<ProviderService> serviceRoutes) {
        // 1. 获取当前客户端ip
        String realIp = IpUtil.getRealIp();
        // 2.计算ip的hashcode
        int hashCode = realIp.hashCode();
        // 3. 求余数寻找要调用的服务器下标,取绝对值是因为hashCode有可能是负数
        int index = Math.abs(hashCode % serviceRoutes.size());
        // 4. 返回指定下标的服务节点
        return serviceRoutes.get(index);
    }
}
