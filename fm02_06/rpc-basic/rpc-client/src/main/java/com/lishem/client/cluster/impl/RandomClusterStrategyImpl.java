package com.lishem.client.cluster.impl;

import com.lishem.client.channel.ProviderService;
import com.lishem.client.cluster.ClusterStrategy;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 随机选取可调用的服务进行请求
 */
@Component("random")
public class RandomClusterStrategyImpl implements ClusterStrategy {

    @Override
    public ProviderService select(List<ProviderService> serviceRoutes) {
        // 1. 获取服务节点的最大长度
        int MAX_LEN = serviceRoutes.size();
        // 2. 确定选择节点的下标（随机确定下标，注意范围）
        int index = RandomUtils.nextInt(0, MAX_LEN);
        // 获取指定下标节点
        return serviceRoutes.get(index);
    }
}
