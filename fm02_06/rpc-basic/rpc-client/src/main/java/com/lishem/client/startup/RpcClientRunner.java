package com.lishem.client.startup;

import com.lishem.client.cache.ServiceRouteCache;
import com.lishem.client.config.RpcClientConfiguration;
import com.lishem.client.proxy.ServiceProxyManager;
import com.lishem.client.runner.RpcRequestManager;
import com.lishem.client.runner.RpcRequestPool;
import com.lishem.client.zk.ServicePullManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Rpc客户端启动实现
 */
@Component
public class RpcClientRunner {

    @Autowired
    private ServicePullManager servicePullManager;

    @Autowired
    private ServiceProxyManager serviceProxyManager;

    @Autowired
    private ServiceRouteCache serviceRouteCache;

    @Autowired
    private RpcClientConfiguration rpcClientConfiguration;

    @Autowired
    private RpcRequestPool rpcRequestPool;

    public void run() {
        // 从Zookeeper拉取服务数据
        servicePullManager.pullServiceFromZK();

        // 为每个服务创建代理，并注入到SpringIOC容器中
        serviceProxyManager.initServiceProxyInstance();

        // 启动Netty客户端服务
        RpcRequestManager.startRpcRequestManager(serviceRouteCache,rpcClientConfiguration,rpcRequestPool);
    }
}
