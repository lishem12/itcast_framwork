package com.lishem.client.runner;

import com.lishem.common.data.RpcResponse;
import io.netty.util.concurrent.DefaultPromise;
import io.netty.util.concurrent.EventExecutor;
import io.netty.util.concurrent.Promise;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Rpc异步 每次请求的消息处理
 */
@Component
public class RpcRequestPool {

    // 定义一个集合，存储每次的消息 ->Future(Promise)
    private final ConcurrentHashMap<String, Promise<RpcResponse>> requestPool = new ConcurrentHashMap<>();

    /**
     * 注册请求的Promise
     * @param requestId
     * @param executor
     */
    public void submitRequest(String requestId, EventExecutor executor) {
        // executor： 监听操作 比如 setSuccess(),就可以用get获取数据
        requestPool.put(requestId, new DefaultPromise<>(executor));
    }

    /**
     * 从集合中获取指定请求(requestId)的消息
     * @param requestId
     * @return
     * @throws Exception
     */
    public RpcResponse fetchResponse(String requestId) throws Exception {
        Promise<RpcResponse> promise = requestPool.get(requestId);
        if (promise == null) {
            return null;
        }
        RpcResponse RpcResponse = promise.get(10, TimeUnit.SECONDS);
        requestPool.remove(requestId);

        RpcRequestManager.destroyChannelHolder(requestId);
        return RpcResponse;
    }

    /**
     * 从RpcResponseHandler中把消息提取出来寸入到Future(Promise)
     * @param requestId
     * @param RpcResponse
     */
    public void notifyRequest(String requestId, RpcResponse RpcResponse) {
        Promise<RpcResponse> promise = requestPool.get(requestId);
        if (promise != null) {
            promise.setSuccess(RpcResponse);
        }
    }
}