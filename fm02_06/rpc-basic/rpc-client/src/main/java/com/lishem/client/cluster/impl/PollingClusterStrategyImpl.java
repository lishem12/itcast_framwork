package com.lishem.client.cluster.impl;

import com.lishem.client.channel.ProviderService;
import com.lishem.client.cluster.ClusterStrategy;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 轮训策略
 */
@Component("polling")
public class PollingClusterStrategyImpl implements ClusterStrategy {

    // 全局下标
    private int counter = 0;

    private Lock lock = new ReentrantLock();

    /**
     * 轮训策略
     * @param serviceRoutes
     * @return
     */
    @Override
    public ProviderService select(List<ProviderService> serviceRoutes) {
        ProviderService providerService = null;
        try {
            lock.tryLock(10, TimeUnit.SECONDS);
            int size = serviceRoutes.size();
            // 下标是否超过服务节点集合的下标，超过了需要归零
            if (counter >= size) {
                counter = 0;
            }

            providerService = serviceRoutes.get(counter);
            counter++;
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            lock.unlock();
        }

        if (providerService == null) {
            providerService = serviceRoutes.get(0);
        }
        return providerService;
    }
}
