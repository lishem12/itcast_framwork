package com.lishem.client.cache;

import com.google.common.cache.LoadingCache;
import com.lishem.client.channel.ProviderService;
import com.lishem.client.zk.ZKit;
import com.lishem.common.annotation.RpcClient;
import com.lishem.common.utils.RpcException;
import com.lishem.common.utils.SpringBeanFactory;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Component
public class ServiceRouteCache {

    /**
     * 存储某接口和提供服务的所有地址信息
     */
    @Autowired
    private LoadingCache<String, List<ProviderService>> cache;

    @Autowired
    private ZKit zKit;

    /**
     * 添加接口和服务地址
     * @param serviceName
     * @param serviceRoutes
     */
    public void addCache(String serviceName, List<ProviderService> serviceRoutes) {
        cache.put(serviceName, serviceRoutes);
    }

    /**
     * 更新接口和服务地址
     * @param serviceName
     * @param serviceRoutes
     */
    public void updateCache(String serviceName, List<ProviderService> serviceRoutes) {
        cache.put(serviceName, serviceRoutes);
    }

    /**
     * 更新所有接口和服务地址
     * @param newServiceRoutesMap
     */
    public void updateCache(Map<String, List<ProviderService>> newServiceRoutesMap) {
        cache.invalidateAll();
        for (Map.Entry<String, List<ProviderService>> entry : newServiceRoutesMap.entrySet()) {
            cache.put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * 通过服务接口名获取服务地址
     * @param serviceName
     * @return
     */
    public List<ProviderService> getServiceRoutes(String serviceName) {
        if (cache.size() == 0) {
            reloadCache();

            if (cache.size() == 0) {
                throw new RpcException("Not any service which is available.");
            }
        }
        try {
            return cache.get(serviceName);
        } catch (ExecutionException e) {
            throw new RpcException(e);
        }
    }

    /**
     * 重新加载缓存
     */
    private void reloadCache() {
        Map<String, Object> beans = SpringBeanFactory.getBeanListByAnnotationClass(RpcClient.class);
        if (MapUtils.isEmpty(beans)) {
            return;
        }
        for (Object bean : beans.values()) {
            String serviceName = bean.getClass().getName();
            List<ProviderService> serviceRoutes = zKit.getServiceInfos(serviceName);
            addCache(serviceName, serviceRoutes);
        }
    }
}

