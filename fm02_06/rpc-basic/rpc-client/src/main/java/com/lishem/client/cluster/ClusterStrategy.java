package com.lishem.client.cluster;

import com.lishem.client.channel.ProviderService;

import java.util.List;

/**
 * 负载均衡接口
 */
public interface ClusterStrategy {

    /**
     * 选择指定的负载均衡，来确定调用哪个服务
     */
    ProviderService select(List<ProviderService> serviceRoutes);
}
