package com.lishem.client.runner;

import com.lishem.client.cache.ServiceRouteCache;
import com.lishem.client.channel.ChannelHolder;
import com.lishem.client.channel.ProviderService;
import com.lishem.client.cluster.ClusterStrategy;
import com.lishem.client.cluster.engin.ClusterEngine;
import com.lishem.client.config.RpcClientConfiguration;
import com.lishem.client.connector.RpcClientConnector;
import com.lishem.client.connector.RpcClientInitializer;
import com.lishem.common.data.RpcRequest;
import com.lishem.common.data.RpcResponse;
import com.lishem.common.utils.RpcException;
import com.lishem.common.utils.SpringBeanFactory;
import com.lishem.common.utils.StatusEnum;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Rpc请求管理器
 */
@Slf4j
public class RpcRequestManager {

    // 服务接口与 提供服务地址缓存
    private static ServiceRouteCache SERVICE_ROUTE_CACHE;

    // Netty pipline 链
    private static RpcClientInitializer rpcClientInitializer;

    // 负载均衡策略
    private static String CLUSTER_STRATEGY;

    // 回调消息处理对象(异步)
    private static RpcRequestPool REQUEST_POOL;

    public static void startRpcRequestManager(ServiceRouteCache serviceRouteCache) {
        SERVICE_ROUTE_CACHE = serviceRouteCache;
        rpcClientInitializer = SpringBeanFactory.getBean(RpcClientInitializer.class);
    }

    /**
     * 调用线程池配置 （异步）
     */
    private static final ExecutorService REQUEST_EXECUTOR = new ThreadPoolExecutor(
            30,
            100,
            0,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(30),
            new BasicThreadFactory.Builder().namingPattern("request-service-connector-%d").build()
    );

    /**
     * 将配置传入的静态方法
     *
     * @param serviceRouteCache
     * @param configuration
     */
    public static void startRpcRequestManager(ServiceRouteCache serviceRouteCache,
                                              RpcClientConfiguration configuration,
                                              RpcRequestPool rpcRequestPool) {
        SERVICE_ROUTE_CACHE = serviceRouteCache;
        rpcClientInitializer = SpringBeanFactory.getBean(RpcClientInitializer.class);
        // 存在策略对象,从IOC容器中取出来
        CLUSTER_STRATEGY = SpringBeanFactory.getBean(configuration.getRpcClientClusterStrategy());
        REQUEST_POOL = rpcRequestPool;
    }

    /**
     * 定义一个集合，存储每次(唯一ID requestId)请求对应的ChannelHolder对象 （异步）
     */
    private static final Map<String, ChannelHolder> channelHolderMap = new ConcurrentHashMap<>();

    /**
     * 绑定会话的ChannelHolder对象（异步调用）
     *
     * @param requestId
     * @param channelHolder
     */
    public static void registerChannelHolder(String requestId, ChannelHolder channelHolder) {
        if (StringUtils.isBlank(requestId) || channelHolder == null) {
            return;
        }
        log.info("Register ChannelHolder[{}:{}] successfully", requestId, channelHolder.toString());
        channelHolderMap.put(requestId, channelHolder);
        // 向RpcRequestPool注册一个请求对应的消息回调对象Future(Promise)
        REQUEST_POOL.submitRequest(requestId,channelHolder.getChannel().eventLoop());
    }


    /**
     * 销毁对应会话的ChannelHolder对象 （异步调用）
     *
     * @param requestId
     */
    public static void destroyChannelHolder(String requestId) {
        if (StringUtils.isBlank(requestId)) {
            return;
        }
        ChannelHolder channelHolder = channelHolderMap.remove(requestId);
        channelHolder.getChannel().closeFuture();
        channelHolder.getEventLoopGroup().shutdownGracefully();
    }

    /**
     * 发送客户端请求(异步)
     *
     * @param rpcRequest
     * @throws InterruptedException
     * @throws RpcException
     */
    public static void sendRequestAsync(RpcRequest rpcRequest) throws InterruptedException, RpcException {
        // 1. 根据系统配置， 读取对应的负载均衡策略
        ClusterStrategy strategy = ClusterEngine.queryClusterStrategy(CLUSTER_STRATEGY);
        // 2. 根据类信息， 获取服务接口的配置信息
        List<ProviderService> providerServices = SERVICE_ROUTE_CACHE.getServiceRoutes(rpcRequest.getClassName());
        // 3. 根据负载均衡策略，选取对应的实现引擎
        ProviderService targetServiceProvider = strategy.select(providerServices);

        if (targetServiceProvider != null) {
            // --------------异步发消息start-------------
            String requestId = rpcRequest.getRequestId();
            // 1） 获取连接
            CountDownLatch latch = new CountDownLatch(1);
            REQUEST_EXECUTOR.execute(new RpcClientConnector(requestId,targetServiceProvider,latch));
            // 计数器挂起，等待计数器到0
            latch.await();
            // 2) 发消息
            ChannelHolder channelHolder = channelHolderMap.get(requestId);
            channelHolder.getChannel().writeAndFlush(rpcRequest);
            log.info("Send request[{}:{}] to service provider successfully", requestId, rpcRequest.toString());
            // --------------异步发消息end--------------
        }else{
            throw new RpcException("没找到服务器？？？暂时这样写");
        }
    }


    /**
     * 发送客户端请求(同步机制)
     *
     * @param rpcRequest
     * @throws InterruptedException
     * @throws RpcException
     */
    public static RpcResponse sendRequest(RpcRequest rpcRequest) throws InterruptedException, RpcException {
        // 1. 根据系统配置， 读取对应的负载均衡策略
        ClusterStrategy strategy = ClusterEngine.queryClusterStrategy(CLUSTER_STRATEGY);
        // 2. 根据类信息， 获取服务接口的配置信息
        List<ProviderService> providerServices = SERVICE_ROUTE_CACHE.getServiceRoutes(rpcRequest.getClassName());
        // 3. 根据负载均衡策略，选取对应的实现引擎
        ProviderService targetServiceProvider = strategy.select(providerServices);

        if (targetServiceProvider != null) {
            String requestId = rpcRequest.getRequestId();
            // 3. 发起远程调用
            RpcResponse response = requestByNetty(rpcRequest, targetServiceProvider);
            log.info("Send request[{}:{}] to service provider successfully", requestId, rpcRequest.toString());
            return response;
        } else {
            throw new RpcException(StatusEnum.NOT_FOUND_SERVICE_PROVIDER);
        }
    }

    /**
     * 采用Netty进行远程调用
     */
    public static RpcResponse requestByNetty(RpcRequest rpcRequest, ProviderService providerService) {

        // 1. 创建Netty连接配置
        EventLoopGroup worker = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(worker)
                .channel(NioSocketChannel.class)
                .remoteAddress(providerService.getServerIp(), providerService.getNetworkPort())
                .handler(rpcClientInitializer);
        try {
            // 2. 建立连接
            ChannelFuture future = bootstrap.connect().sync();
            if (future.isSuccess()) {
                ChannelHolder channelHolder = ChannelHolder.builder()
                        .channel(future.channel())
                        .eventLoopGroup(worker)
                        .build();
                log.info("Construct a connector with service provider[{}:{}] successfully",
                        providerService.getServerIp(),
                        providerService.getNetworkPort()
                );

                // 3. 创建请求回调对象
                final RequestFuture<RpcResponse> responseFuture = new SyncRequestFuture(rpcRequest.getRequestId());
                // 4. 将请求回调放置缓存
                SyncRequestFuture.syncRequest.put(rpcRequest.getRequestId(), responseFuture);
                // 5. 根据连接通道， 下发请求信息
                ChannelFuture channelFuture = channelHolder.getChannel().writeAndFlush(rpcRequest);
                // 6. 建立回调监听
                channelFuture.addListener(new ChannelFutureListener() {
                    @Override
                    public void operationComplete(ChannelFuture future) throws Exception {
                        // 7. 设置是否成功的标记
                        responseFuture.setWriteResult(future.isSuccess());
                        if (!future.isSuccess()) {
                            // 调用失败，清除连接缓存
                            SyncRequestFuture.syncRequest.remove(responseFuture.requestId());
                        }
                    }
                });
                // 8. 阻塞等待3秒
                RpcResponse result = responseFuture.get(3, TimeUnit.SECONDS);
                // 9. 移除连接缓存
                SyncRequestFuture.syncRequest.remove(rpcRequest.getRequestId());

                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
