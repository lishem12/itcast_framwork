package com.lishem.client.cluster.impl;


import com.lishem.client.channel.ProviderService;
import com.lishem.client.cluster.ClusterStrategy;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 随机权重
 */
@Component("weight_random")
public class WeightRandomClusterStrategyImpl implements ClusterStrategy {

    /**
     * 随机 + 权重
     *
     * @param serviceRoutes
     * @return
     */
    @Override
    public ProviderService select(List<ProviderService> serviceRoutes) {
        // 1. 新建一个集合存储具有选中概率的数组
        List<ProviderService> newProviderList = new ArrayList<>();
        // 2. 循环所有节点
        for (ProviderService serviceRoute : serviceRoutes) {
            // 3. 获取每个节点权重
            int weight = serviceRoute.getWeight();
            // 4. 往新集合中添加权重个节点个数
            for (int i = 0; i < weight; i++) {
                newProviderList.add(serviceRoute);
            }
        }

        // 随机选择
        int MAX_LEN = newProviderList.size();
        int index = RandomUtils.nextInt(0, MAX_LEN);
        return newProviderList.get(index);
    }
}
