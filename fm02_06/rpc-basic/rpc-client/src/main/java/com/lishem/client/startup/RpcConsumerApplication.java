package com.lishem.client.startup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 客户端启动入口
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.lishem"})
public class RpcConsumerApplication implements ApplicationRunner {

    @Autowired
    private RpcClientRunner rpcClientRunner;

    public static void main(String[] args) {
        SpringApplication.run(RpcConsumerApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        rpcClientRunner.run();
    }
}

