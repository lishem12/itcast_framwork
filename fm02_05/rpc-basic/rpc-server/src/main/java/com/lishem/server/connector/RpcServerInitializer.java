package com.lishem.server.connector;

import com.lishem.server.connector.handler.RpcRequestHandler;
import com.lishem.server.connector.handler.RpcServerDecodeHandler;
import com.lishem.server.connector.handler.RpcServerEncodeHandler;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ChannelHandler.Sharable
public class RpcServerInitializer extends ChannelInitializer<Channel> {

    @Autowired
    private RpcRequestHandler znsRequestHandler;

    /**
     * 初始化连接通道
     * @param channel
     * @throws Exception
     */
    @Override
    protected void initChannel(Channel channel) throws Exception {
        channel.pipeline()
                .addLast(new RpcServerDecodeHandler())
                .addLast(new RpcServerEncodeHandler())
                .addLast(znsRequestHandler);
    }
}
