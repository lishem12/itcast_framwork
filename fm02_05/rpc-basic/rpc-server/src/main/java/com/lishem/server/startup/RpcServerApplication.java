package com.lishem.server.startup;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.lishem"})
@SpringBootApplication
@Slf4j
public class RpcServerApplication implements ApplicationRunner {

    @Autowired
    private RpcServerRunner znsServerRunner;

    public static void main(String[] args) {
        SpringApplication.run(RpcServerApplication.class, args);

        log.info("Zns service provider application startup successfully");

    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        znsServerRunner.run();
    }
}