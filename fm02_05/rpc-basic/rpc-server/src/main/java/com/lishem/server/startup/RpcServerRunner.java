package com.lishem.server.startup;


import com.lishem.server.connector.RpcServerAcceptor;
import com.lishem.server.zk.ServicePushManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Rpc服务端启动实现
 */
@Component
@Slf4j
public class RpcServerRunner {


    private static ExecutorService executor = null;

    @Autowired
    private ServicePushManager servicePushManager;

    public void run() {
        executor = Executors.newFixedThreadPool(5);

        // Start Acceptor，waiting for the service caller to fire the request call
        executor.execute(new RpcServerAcceptor());

        // Register service providers into Zookeeper
        servicePushManager.registerIntoZK();
    }


    @PreDestroy
    public void destroy() {
        if (executor != null) {
            executor.shutdown();
        }
    }
}
