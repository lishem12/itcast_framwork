package com.lishem.server.zk;

import com.lishem.common.annotation.RpcService;
import com.lishem.server.config.RpcServerConfiguration;
import com.lishem.common.utils.IpUtil;
import com.lishem.common.utils.SpringBeanFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Zookeeper服务连接注册管理
 */
@Component
@Slf4j
public class ServicePushManager {

    @Autowired
    private ZKit zKit;

    @Autowired
    private RpcServerConfiguration configuration;

    /**
     * 服务注册接口
     */
    public void registerIntoZK() {
        // 1.找到所有有@RpcService注解的类
        Map<String, Object> beanListByAnnotationClass =
                SpringBeanFactory.getBeanListByAnnotationClass(RpcService.class);
        if (!MapUtils.isEmpty(beanListByAnnotationClass)) {
            // 创建根节点
            zKit.createRootNode();

            // 创建接口节点
            for (Object bean : beanListByAnnotationClass.values()) {
                // 2.获取每个类上的注解@RpcService.cls的值
                RpcService annotation = bean.getClass().getAnnotation(RpcService.class);
                Class<?> clazz = annotation.cls();

                // create /aaa/bbb/ccc
                // 3. 获取cls的值之后，将它的name作为节点的名字，注册到Zookeeper中(在rpc节点下创建一个子节点)
                String serviceName = clazz.getName();

                // 创建接口对应节点
                zKit.createPersistentNode(serviceName);
                // 3. 同时为每个结点创建一个子节点 Ip:HttpPort:RpcPort
                String serviceAddress =
                        IpUtil.getRealIp()+
                                ":"+configuration.getServerPort()+
                                ":"+configuration.getNetworkPort();
                zKit.createNode(serviceName+"/"+serviceAddress);
            }
        }
    }
}
