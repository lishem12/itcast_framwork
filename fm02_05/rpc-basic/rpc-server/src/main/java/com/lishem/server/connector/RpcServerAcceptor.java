package com.lishem.server.connector;

import com.lishem.server.config.RpcServerConfiguration;
import com.lishem.common.utils.SpringBeanFactory;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * Rpc服务端连接接收器
 */
@Slf4j
public class RpcServerAcceptor implements Runnable {


    private EventLoopGroup boss = new NioEventLoopGroup();
    private EventLoopGroup worker = new NioEventLoopGroup();

    private RpcServerConfiguration znsServerConfiguration;
    private RpcServerInitializer znsServerInitializer;

    public RpcServerAcceptor() {
        this.znsServerConfiguration = SpringBeanFactory.getBean(RpcServerConfiguration.class);
        this.znsServerInitializer = SpringBeanFactory.getBean(RpcServerInitializer.class);
    }

    /**
     * Netty通讯服务启动
     */
    @Override
    public void run() {
        // 1. Netty服务配置
        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(boss, worker)
                .channel(NioServerSocketChannel.class)
                .handler(new LoggingHandler(LogLevel.DEBUG))
                .option(ChannelOption.SO_BACKLOG, 1024)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childHandler(znsServerInitializer);

        try {
            log.info("ZnsServer acceptor startup at port[{}] successfully", znsServerConfiguration.getNetworkPort());
            // 2. 绑定端口， 启动服务
            ChannelFuture future = bootstrap.bind(znsServerConfiguration.getNetworkPort()).sync();
            // 3. 服务同步阻塞方式运行
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            log.error("ZnsServer acceptor startup failure!", e);
            e.printStackTrace();
        } finally {
            boss.shutdownGracefully().syncUninterruptibly();
            worker.shutdownGracefully().syncUninterruptibly();
        }
    }
}
