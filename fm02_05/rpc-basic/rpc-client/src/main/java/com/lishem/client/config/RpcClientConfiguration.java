package com.lishem.client.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class RpcClientConfiguration {

    // 根路径
    @Value("${rpc.client.zk.root}")
    private String zkRoot;

    // zookeeper 地址
    @Value("${rpc.client.zk.addr}")
    private String zkAddr;

    // 服务端口
    @Value("${server.port}")
    private String znsClientPort;

    // 被提供服务的包名
    @Value("${rpc.client.api.package}")
    private String rpcClientApiPackage;

    // 负载均衡策略
    @Value("${rpc.cluster.strategy}")
    private String rpcClientClusterStrategy;

    // 请求超时时间
    @Value("${rpc.client.zk.timeout}")
    private Integer connectTimeout;
}
