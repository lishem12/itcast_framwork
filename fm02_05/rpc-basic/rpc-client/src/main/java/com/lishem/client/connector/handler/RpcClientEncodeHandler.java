package com.lishem.client.connector.handler;

import com.lishem.common.data.RpcRequest;
import com.lishem.common.utils.ProtoSerializerUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * Rpc客户端编码器
 */
public class RpcClientEncodeHandler extends MessageToByteEncoder<RpcRequest> {

    @Override
    protected void encode(ChannelHandlerContext ctx, RpcRequest RpcRequest, ByteBuf in) throws Exception {
        // 调用封装的protostuff公用组件， 实现序列化
        byte[] bytes = ProtoSerializerUtil.serialize(RpcRequest);
        in.writeInt(bytes.length);
        in.writeBytes(bytes);
    }
}