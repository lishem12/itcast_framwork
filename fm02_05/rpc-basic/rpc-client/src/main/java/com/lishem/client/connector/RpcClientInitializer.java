package com.lishem.client.connector;

import com.lishem.client.connector.handler.RpcClientDecodeHandler;
import com.lishem.client.connector.handler.RpcClientEncodeHandler;
import com.lishem.client.connector.handler.RpcResponseHandler;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ChannelHandler.Sharable
public class RpcClientInitializer extends ChannelInitializer<Channel> {

    @Autowired
    private RpcResponseHandler znsResponseHandler;

    @Override
    protected void initChannel(Channel channel) throws Exception {
        channel.pipeline()
                .addLast(new RpcClientEncodeHandler())
                .addLast(new RpcClientDecodeHandler())
                .addLast(znsResponseHandler);
    }
}
