package com.lishem.client.channel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用于存储远程提供服务的地址数据
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProviderService implements Serializable {
    private String serverIp;
    private int serverPort;
    private int networkPort;

    private long timeout;
    // the weight of service provider
    private int weight;
}