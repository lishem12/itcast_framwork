package com.lishem.api;

import com.lishem.common.annotation.RpcClient;

@RpcClient
public interface OrderService {

    /**
     * 获取订单信息
     * @param userName
     * @return
     */
    String getOrder(String userName, String orderNo);
}