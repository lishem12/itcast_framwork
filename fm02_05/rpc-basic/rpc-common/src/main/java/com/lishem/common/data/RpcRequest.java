package com.lishem.common.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 请求对象
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RpcRequest {
    private String requestId;
    private String className; // 接口类名
    private String methodName; // 接口方法名
    private Class<?>[] parameterTypes; // 接口参数列表
    private Object[] parameters;
}