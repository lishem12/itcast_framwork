package com.lishem.MyServlet;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

public class TestServlet implements Servlet {
    @Override
    public void init(ServletConfig config) throws ServletException {

    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        String method = ((HttpServletRequest) req).getMethod();
        res.setContentType("text/html");
        if (Objects.equals("GET",method)){
            // get请求
            res.getWriter().println("<h1> get request<h1>");
        }else if(Objects.equals("POST",method)){
            // post 请求
            res.getWriter().println("<h1> post request<h1>");
        }else {
            // post 请求
            res.getWriter().println("<h1> other request<h1>");
        }
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {

    }
}
