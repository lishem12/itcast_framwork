package com.lishem.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MyRequest {

    private String url;
    private String method;

    public MyRequest(InputStream inputStream) throws IOException {
        String httpRequest = getStrFromInputStream(inputStream);
        /**
         * GET /hello HTTP/1.1
         * Host: localhost:9999
         * Connection: keep-alive
         * Cache-Control: max-age=0
         * Upgrade-Insecure-Requests: 1
         * User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36
         * Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng;q=0.8,application/signed-exchange;v=b3;q=0.9
         * Sec-Fetch-Site: cross-site
         */
        String httpHead = httpRequest.split("\n")[0];
        method = httpHead.split("\\s")[0]; // GET
        url = httpHead.split("\\s")[1]; // /hello
    }

    private String getStrFromInputStream(InputStream inputStream) throws IOException {
        InputStreamReader r = new InputStreamReader(inputStream);
        BufferedReader br = new BufferedReader(r);
        return br.readLine();
    }

    public String getUrl() {
        return url;
    }

    public String getMethod() {
        return method;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
