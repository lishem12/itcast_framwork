package com.lishem.servlet;

import java.io.OutputStream;
import java.io.PrintWriter;

public class MyResponse {
    private OutputStream outputStream;

    public MyResponse(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public void write(String content) {
        String html = "http/1.1 200 ok\n" + "Content-type: text/html; charset=utf-8\n" + "\n\n" + "<!DOCTYPE html>\n" + "<html><body>"
                + content + "</body></html>";
        PrintWriter out = new PrintWriter(outputStream);
        out.println(html);
        out.flush();
        out.close();
    }
}