package com.lishem.servlet;

public  abstract class MyServlet {

    public void doGet(MyRequest request, MyResponse response) {
        // 这里其实应该是response返回405这种状态码，因为我们的MyResponse没有这个功能，所以这里直接抛异常了
        throw new RuntimeException("不支持的HTTP GET method");
    }

    public void doPost(MyRequest request, MyResponse response) {
        // 这里其实应该是response返回405这种状态码，因为我们的MyResponse没有这个功能，所以这里直接抛异常了
        throw new RuntimeException("不支持的HTTP POST method");
    }

    public void service(MyRequest request, MyResponse response) {
        String method = request.getMethod();
        if (method.equalsIgnoreCase("POST")) {
            doPost(request, response);
        } else if (method.equalsIgnoreCase("GET")) {
            doGet(request, response);
        }
    }
}
