package com.lishem.servletconf;

import java.util.ArrayList;
import java.util.List;

public class MyServletMappingConfig {

    public static List<MyServletMapping> myServletMappingList = new ArrayList<>();

    static{
        /**
         * 这个映射关系，Tomcat应该是读取web.xml配置获取的，这里为了方便，我们直接写死了
         */
        myServletMappingList.add(new MyServletMapping("helloworld","/hello","com.lishem.test.HelloWorldServlet"));
    }

}
