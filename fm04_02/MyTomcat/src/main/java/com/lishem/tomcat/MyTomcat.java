package com.lishem.tomcat;

import com.lishem.servlet.MyRequest;
import com.lishem.servlet.MyResponse;
import com.lishem.servlet.MyServlet;
import com.lishem.servletconf.MyServletMapping;
import com.lishem.servletconf.MyServletMappingConfig;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class MyTomcat {

    private int port;

    private Map<String, String> url2ServletMap = new HashMap<>();

    public MyTomcat(int port) {
        this.port = port;
    }

    public void start() {
        initServletMapping();
        try {
            // 1. 端口监听
            ServerSocket server = new ServerSocket(9999);
            System.out.println("MyTomcat start on port " + port);

            while (true) {
                Socket socket = server.accept();
                // 2. 封装Request、Response
                MyRequest request = new MyRequest(socket.getInputStream());
                MyResponse response = new MyResponse(socket.getOutputStream());

                // 3. 请求分发
                dispatch(request, response);
                socket.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void dispatch(MyRequest request, MyResponse response) {
        String servletClazz = url2ServletMap.get(request.getUrl());
        try {
            Class<MyServlet> myServletClass = (Class<MyServlet>) Class.forName(servletClazz);
            MyServlet myServlet = myServletClass.newInstance();
            myServlet.service(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void initServletMapping() {
        for (MyServletMapping myServletMapping : MyServletMappingConfig.myServletMappingList) {
            url2ServletMap.put(myServletMapping.getUrl(), myServletMapping.getClazz());
        }
    }


}
