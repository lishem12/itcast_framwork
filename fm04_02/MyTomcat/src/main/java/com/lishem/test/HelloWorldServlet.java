package com.lishem.test;

import com.lishem.servlet.MyRequest;
import com.lishem.servlet.MyResponse;
import com.lishem.servlet.MyServlet;

public class HelloWorldServlet extends MyServlet {
    @Override
    public void doGet(MyRequest request, MyResponse response) {
        response.write("hello world");
    }
}
