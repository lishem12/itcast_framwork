package com.lishem.pattern.decorator.commonDecorator;

public interface Shape {

    void draw();
}