package com.lishem.pattern.decorator.commonDecorator;

public class DecoratorTest {

    public static void main(String[] args) {
        //被装饰对象
        Circle circle = new Circle();
        //创建装饰者对象
        RedShapeDecorator rd = new RedShapeDecorator(circle);
        //调用方法
        rd.draw();
    }
}
