package com.lishem.pattern.decorator.dubboAOP;

import org.apache.dubbo.common.extension.SPI;

@SPI
public interface Phone {
    void call();
}