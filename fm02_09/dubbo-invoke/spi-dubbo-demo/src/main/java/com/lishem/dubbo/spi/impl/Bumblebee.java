package com.lishem.dubbo.spi.impl;


import com.lishem.dubbo.spi.Robot;

public class Bumblebee implements Robot {

    @Override
    public void sayHello() {
        System.out.println("Hello, I am Bumblebee.");
    }
}
