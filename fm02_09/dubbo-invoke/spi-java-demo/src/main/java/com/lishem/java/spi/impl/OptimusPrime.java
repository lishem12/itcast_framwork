package com.lishem.java.spi.impl;

import com.lishem.java.spi.Robot;

public class OptimusPrime implements Robot {

    @Override
    public void sayHello() {
        System.out.println("Hello, I am Optimus Prime.");
    }
}
