package com.lishem.dubbo.spring.consumer.controller;

import com.lishem.dubbo.spring.api.OrderService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OrderController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 订单服务接口
     */
    //@DubboReference(version = "${dubbo.spring.provider.version}")
    @DubboReference(
            version = "${dubbo.spring.provider.version}"
            ,retries =  3
            ,loadbalance = "random"
            ,stub = "com.lishem.dubbo.spring.stub.OrderServiceStub"
//            ,timeout = 1000
//            , methods = {
//            @Method(name = "getOrder", timeout = 1000)
//    }
    )
    private OrderService orderService;

    /**
     * 获取订单详情接口
     *
     * @param orderId
     * @return
     */
    @RequestMapping("/getOrder")
    @ResponseBody
    public String getOrder(Long orderId) {
        String result = null;
        try {
            result = orderService.getOrder(orderId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}