package com.lishem.dubbo.spring.consumer.config;

import org.apache.dubbo.config.ConsumerConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DubboConfig {

    /**
     * 客户端全局操作
     */
    @Bean
    public ConsumerConfig  config(){
        ConsumerConfig config = new ConsumerConfig();
        // config.setTimeout(4000);
        // 负载均衡

        config.setLoadbalance("roundrobin"); // 轮训
        config.setLoadbalance("random"); // 随机
        config.setLoadbalance("consistenthash"); // 一致性哈希
        config.setLoadbalance("leastactive"); // 平均处理效率最高的节点
        config.setLoadbalance("shortestresponse"); // 最后一次相应时间最快的
        return config;
    }
}
