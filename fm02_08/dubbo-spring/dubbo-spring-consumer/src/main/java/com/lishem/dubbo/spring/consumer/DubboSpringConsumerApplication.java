package com.lishem.dubbo.spring.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.lishem"})
public class DubboSpringConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboSpringConsumerApplication.class, args);
    }
}
