package com.lishem.dubbo.spring.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.lishem"})
public class DubboSpringProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboSpringProviderApplication.class, args);
    }
}