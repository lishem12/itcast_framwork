package com.lishem.dubbo.spring.provider.config;

import org.apache.dubbo.config.ProviderConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DubboConfig {

    /**
     * 服务端全局配置
     */
    public ProviderConfig registryConfig() {
        ProviderConfig config = new ProviderConfig();
        // 全局超时时间 1S
        // config.setTimeout(1000);
        return config;
    }
}
