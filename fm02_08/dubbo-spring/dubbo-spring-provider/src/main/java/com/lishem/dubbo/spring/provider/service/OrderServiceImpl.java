package com.lishem.dubbo.spring.provider.service;

import com.lishem.dubbo.spring.api.OrderService;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.config.annotation.Method;
import org.springframework.beans.factory.annotation.Value;


@DubboService(
        version = "${dubbo.spring.provider.version}"
        //
        //,methods = {
        //        // 某个方法配置
        //        @Method(name = "getOrder", timeout = 2000)
        //}
)
public class OrderServiceImpl implements OrderService {

    /**
     * 服务端口
     */
    @Value("${server.port}")
    private String serverPort;

    /**
     * 获取订单详情
     *
     * @param orderId
     * @return
     */
    @Override
    public String getOrder(Long orderId) {
        try {
            Thread.sleep(900);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Service:" + serverPort);
        return "Get Order Detail, Id: " + orderId + ", serverPort: " +
                serverPort;
    }
}
