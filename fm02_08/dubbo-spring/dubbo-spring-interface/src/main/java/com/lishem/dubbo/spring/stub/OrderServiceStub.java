package com.lishem.dubbo.spring.stub;

import com.lishem.dubbo.spring.api.OrderService;

public class OrderServiceStub implements OrderService {

    private final OrderService orderService;

    // 构造函数传入真正的远程代理对象
    public OrderServiceStub(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public String getOrder(Long orderId) throws InterruptedException {
        // 在客户端执行, 预先验证参数是否合法，等等
        try {
            if (null != orderId) {
                // 远程调用
                return orderService.getOrder(orderId);
            }
            return "参数校验错误！";
        } catch (Exception e) {
            //容错处理
            return "出现错误：" + e.getMessage();
        }
    }
}