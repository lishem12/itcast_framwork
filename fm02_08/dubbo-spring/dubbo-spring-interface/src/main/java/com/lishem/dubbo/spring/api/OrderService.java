package com.lishem.dubbo.spring.api;

public interface OrderService {

    /**
     * 获取订单详情
     * @param orderId
     * @return
     */
    String getOrder(Long orderId) throws InterruptedException;
}
