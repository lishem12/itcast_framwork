package com.lishem.dubbo.pattern.decorator.dubboAOP;

import org.apache.dubbo.common.extension.SPI;

@SPI
public interface Phone {
    void call();
}