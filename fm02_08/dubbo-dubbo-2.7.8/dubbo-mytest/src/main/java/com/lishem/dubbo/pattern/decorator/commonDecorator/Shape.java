package com.lishem.dubbo.pattern.decorator.commonDecorator;

public interface Shape {

    void draw();
}