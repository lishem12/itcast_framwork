package com.lishem.dubbo.spi.impl;


import com.lishem.dubbo.spi.Robot;
import org.apache.dubbo.rpc.Protocol;

public class Bumblebee implements Robot {

    private Protocol protocol;

    //set方法
    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    @Override
    public void sayHello() {
        System.out.println(protocol);
        System.out.println("Hello, I am Bumblebee.");
    }
}
