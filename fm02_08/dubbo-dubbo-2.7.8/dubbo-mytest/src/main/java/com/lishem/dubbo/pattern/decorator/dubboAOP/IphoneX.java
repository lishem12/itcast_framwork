package com.lishem.dubbo.pattern.decorator.dubboAOP;

public class IphoneX implements Phone {

    @Override
    public void call() {
        System.out.println("iphone正在拨打电话");
    }
}