package com.lishem.dubbo.springschema;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SchemaDemo {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring/applicationContext.xml");
        User user = (User) context.getBean("user");
        System.out.println(user);
    }
}
