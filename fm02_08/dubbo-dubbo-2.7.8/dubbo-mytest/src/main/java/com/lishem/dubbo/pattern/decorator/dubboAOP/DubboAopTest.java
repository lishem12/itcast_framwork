package com.lishem.dubbo.pattern.decorator.dubboAOP;

import org.apache.dubbo.common.extension.ExtensionLoader;

public class DubboAopTest {

    public static void main(String[] args) {
        ExtensionLoader<Phone> extensionLoader = ExtensionLoader.getExtensionLoader(Phone.class);
        Phone phone = extensionLoader.getExtension("iphone");
        phone.call();
    }
}
