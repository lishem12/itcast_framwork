package com.lishem.obj.hashcode;

import java.util.ArrayList;
import java.util.List;

/**
 * 检验HashCode是否会重复
 */
public class HashCodeTest {

    //目标：只要发生重复，说明hashcode不是内存地址
    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<Integer>();
        int num = 0;
        for (int i = 0; i < 150000; i++) {
            // 创建新对象
            Object obj = new Object();
            if (integerList.contains(obj.hashCode())){
                num++;
            }else {
                integerList.add(obj.hashCode());
            }
        }
        System.out.println(num + "个hashcode发生重复");
        System.out.println("List合计大小" + integerList.size() + "个");
    }
}
