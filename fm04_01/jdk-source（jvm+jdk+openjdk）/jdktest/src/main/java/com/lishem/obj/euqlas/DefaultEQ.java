package com.lishem.obj.euqlas;

public class DefaultEQ {
    String name;

    public DefaultEQ(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        DefaultEQ eq1 = new DefaultEQ("张三");
        DefaultEQ eq2 = new DefaultEQ("张三");
        DefaultEQ eq3 = eq1;

        //虽然俩对象外面看起来一样，eq和==都不行
        //因为我们没有改写equals，它使用默认object的，也就是内存地址
        System.out.println(eq1.equals(eq2)); // false
        System.out.println(eq1 == eq2); // false

        System.out.println("----");
        //1和3是同一个引用
        System.out.println(eq1.equals(eq3)); // true
        System.out.println(eq1 == eq3); // true

        System.out.println("===");
        //以上是对象，再来看基本类型
        int i1 = 1;
        Integer i2 = 1;
        Integer i = new Integer(1);
        Integer j = new Integer(1);

        Integer k = new Integer(2);

        //只要是基本类型，不管值还是包装成对象，都是直接比较大小
        System.out.println(i.equals(i1));  //比较的是值 true
        System.out.println(i == i1);  //拆箱 , true
        // 封装对象i被拆箱，变为值比较，1==1成立
        //相当于 System.out.println(1==1);

        System.out.println(i.equals(j));  // true
        System.out.println(i == j);   //  比较的是地址，这是俩对象 false

        System.out.println(i2 == i); // i2在常量池里，i在堆里，地址不一样 // false

        System.out.println(i.equals(k));  //1和2，不解释 false
    }
}
