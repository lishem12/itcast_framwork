package com.lishem.obj.euqlas;

public class Intern {

    public static void main(String[] args) {
        String str1 = "张三";//常量池
        String str2 = new String("张三");

        // intern；内存地址是否相等(面试常问)
        System.out.println("str1与str2是否相等>>" +(str1==str2)); // false
        System.out.println("str1与str2是否相等>>" +(str1==str2.intern())); // true
        System.out.println("str1与str2是否相等>>" +(str1==str2)); // false
    }
}
