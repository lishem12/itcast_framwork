package com.lishem.arraylist;

import org.openjdk.jol.info.ClassLayout;

import java.util.ArrayList;

public class ArrayListHeader {

    public static void main(String[] args) {
        int[] i = new int[8];
        ArrayList<Integer> list = new ArrayList(8);
        //将8个int类型依次放入数组和arrayList，注意，一个int占4个字节
        for (int j = 0; j < 8; j++) {
            i[j] = j;
            list.add(j);
        }

        System.out.println(ClassLayout.parseInstance(i).toPrintable());
        System.out.println("=============");
        System.out.println(ClassLayout.parseInstance(list).toPrintable());
        System.out.println(ClassLayout.parseClass(ArrayList.class));
    }
}
