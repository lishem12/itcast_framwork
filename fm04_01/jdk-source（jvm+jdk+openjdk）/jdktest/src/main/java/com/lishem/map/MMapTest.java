package com.lishem.map;

import org.openjdk.jol.info.ClassLayout;

import java.util.HashMap;

public class MMapTest {
    public static void main(String[] args) {
        HashMap<Integer, String> m = new HashMap<Integer, String>();//尾插
        //断点跟踪put
        m.put(1, "001");
        m.put(1, "002");
        m.put(17, "003");//使用17可hash冲突（存储位置相同）

        System.out.println(ClassLayout.parseInstance(m).toPrintable());
        //断点跟踪get
        System.out.println(m.get(1));//返回002（数组查找）
        System.out.println(m.get(17));//返回003（链表查找）
        //断点跟踪remove
        m.remove(1);//移除
        System.out.println(m);
        m.remove(1, "002");//和上面的remove走的同一个代码
    }
}
