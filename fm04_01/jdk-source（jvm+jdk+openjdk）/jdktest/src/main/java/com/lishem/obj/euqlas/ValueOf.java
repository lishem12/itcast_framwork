package com.lishem.obj.euqlas;

public class ValueOf {

    public static void main(String[] args) {
        System.out.println(Integer.valueOf(127) == Integer.valueOf(127));
        System.out.println(Integer.valueOf(128) == Integer.valueOf(128));
    }
}
