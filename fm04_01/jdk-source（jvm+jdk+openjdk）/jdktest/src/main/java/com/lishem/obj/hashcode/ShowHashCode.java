package com.lishem.obj.hashcode;

import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.vm.VM;

/**
 * 打印markword看到hashCode
 */
public class ShowHashCode {

    public static void main(String[] args) {
        ShowHashCode a = new ShowHashCode();
        //jvm的信息
        System.out.println(VM.current().details());
        System.out.println("-------------------------");
        //调用之前打印a对象的头信息
        //以表格的形式打印对象布局
        System.out.println(ClassLayout.parseInstance(a).toPrintable());

        System.out.println("-------------------------");
        //调用后再打印a对象的hashcode值
        System.out.println(Integer.toHexString(a.hashCode()));
        System.out.println(ClassLayout.parseInstance(a).toPrintable());

        System.out.println("-------------------------");
        //有线程加重量级锁的时候，再来看对象头
        new Thread(()->{
            try {
                synchronized (a){
                    Thread.sleep(5000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        System.out.println(Integer.toHexString(a.hashCode()));
        System.out.println(ClassLayout.parseInstance(a).toPrintable());
    }
}
