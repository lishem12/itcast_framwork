package com.lishem.order.service;

import com.lishem.order.pojo.Item;
import com.lishem.order.pojo.Order;

import java.util.List;

public interface OrderService {

    Order submitOrder(Long userId, List<Item> itemList);
}
