package com.lishem.core.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NettyServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(NettyServer.class);

    public static void start(String host, int port) {
        doStart(host, port);
    }

    private static void doStart(String host, int port) {
        // 主线程，不处理任何业务，只接受客户端连接请求
        EventLoopGroup boss = new NioEventLoopGroup(1);
        // 工作线程，默认是CPU*2
        EventLoopGroup worker = new NioEventLoopGroup();

        try {
            // 服务端启动
            ServerBootstrap sbs = new ServerBootstrap();
            sbs.group(boss, worker)
                    .channel(NioServerSocketChannel.class) // 配置server通道
                    .childHandler((new ServerInitializer())); // worker线程处理器

            ChannelFuture future = sbs.bind(host, port).sync();
            LOGGER.info("服务器启动完成，地址为：" + host + ":" + port);
            //等待服务端监听端口关闭
            future.channel().closeFuture().sync();

        } catch (Exception e) {
            LOGGER.error("服务器启动失败！", e);
        } finally {
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }
    }
}
