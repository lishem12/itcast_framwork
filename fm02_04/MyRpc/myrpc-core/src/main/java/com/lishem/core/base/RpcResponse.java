package com.lishem.core.base;

public class RpcResponse extends BaseRpcBean{

    private static final long serialVersionUID = 316225276588124313L;

    private String errorMsg; // 错误信息
    private Object result; // 结果数据

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
