package com.lishem.core.client;


import com.lishem.core.base.RpcRequest;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

public class NettyClient {

    private Channel channel;

    private EventLoopGroup worker;

    public void start(String host, int port) {
        try {
            doStart(host, port);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doStart(String host, int port) throws Exception {
        //定义工作线程组
        this.worker = new NioEventLoopGroup();
        Bootstrap bs = new Bootstrap();
        bs.group(worker)
                .channel(NioSocketChannel.class)
                .handler(new ClientInitializer());

        //连接到远程服务
        ChannelFuture future = bs.connect(host, port).sync();

        //保留channel对象，方便后面通过该通道发送消息
        this.channel = future.channel();
    }


    /**
     * 关闭客户端服务
     */
    public void close() {
        if (null != this.channel && this.channel.isActive()) {
            this.channel.close();
        }
        if (null != this.worker && !this.worker.isShutdown()) {
            this.worker.shutdownGracefully();
        }
    }

    /**
     * 发送消息
     *
     * @param request
     */
    public void sendMsg(RpcRequest request) {
        // 会开一个新线程执行
        // this.channel.writeAndFlush(request);
        // 交给EventLoopGroup执行
        channel.eventLoop().execute(() -> {
            channel.writeAndFlush(request);
        });
    }
}
