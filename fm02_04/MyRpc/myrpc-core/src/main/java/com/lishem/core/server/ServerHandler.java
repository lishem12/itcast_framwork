package com.lishem.core.server;

import com.lishem.core.base.RpcRequest;
import com.lishem.core.base.RpcResponse;
import com.lishem.core.util.ClassUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServerHandler extends SimpleChannelInboundHandler<RpcRequest> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerHandler.class);

    private static final Map<Class<?>, Object> OBJECT_MAP = new HashMap<>();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcRequest request) throws Exception {
        RpcResponse rpcResponse = new RpcResponse();
        rpcResponse.setRequestId(request.getRequestId());

        LOGGER.info("开始处理消息：requestId = " + request.getRequestId());

        try {
            Class<?> clazz = Class.forName(request.getClassName());
            if (!OBJECT_MAP.containsKey(clazz)){
                // 获取接口的实现类，这里只用第一个实现类（第一个存下来就break），忽略其他
                List<Class> allClassByInterface = ClassUtil.getAllClassByInterface(clazz);
                for (Class c : allClassByInterface) {
                    //将对象缓存起来，提升效率
                    OBJECT_MAP.put(clazz, c.getDeclaredConstructor().newInstance());
                    break;
                }
            }

            // 通过反射找到方法执行
            Method method = clazz.getMethod(request.getMethodName(), request.getParameterTypes());
            method.setAccessible(true);
            Object result = method.invoke(OBJECT_MAP.get(clazz), request.getParameters());
            rpcResponse.setResult(result);
        }catch (Exception e){
            LOGGER.error("处理失败... requestId = " + request.getRequestId(), e);
            //出错
            rpcResponse.setErrorMsg("error");
        }
        ctx.writeAndFlush(rpcResponse);
    }
}
