package com.lishem.core.server;

import com.lishem.core.base.RpcRequest;
import com.lishem.core.codec.MyDecoder;
import com.lishem.core.codec.MyEncoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class ServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline()
                .addLast(new MyDecoder(RpcRequest.class)) // 解码器，需要解码的对象是RpcRequest
                .addLast(new MyEncoder()) // 编码器，用于数据响应
                .addLast(new ServerHandler()); // 自定义逻辑
    }
}
