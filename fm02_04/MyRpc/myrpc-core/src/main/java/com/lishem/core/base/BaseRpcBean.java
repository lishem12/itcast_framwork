package com.lishem.core.base;

import java.io.Serializable;

public class BaseRpcBean implements Serializable {

    // 请求id
    private String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
