package com.lishem.core.base;

/**
 * 自定义请求对象
 */
public class RpcRequest extends BaseRpcBean {

    private static final long serialVersionUID = -4599165185347871064L;

    private long currentMillisTime; // 创建请求时间
    private String className; // 类名称，全包路径
    private String methodName; // 执行方法名
    private Class<?>[] parameterTypes; // 方法中传入参数类型
    private Object[] parameters; // 执行方法传入的参数

    public long getCurrentMillisTime() {
        return currentMillisTime;
    }

    public void setCurrentMillisTime(long currentMillisTime) {
        this.currentMillisTime = currentMillisTime;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Class<?>[] getParameterTypes() {
        return parameterTypes;
    }

    public void setParameterTypes(Class<?>[] parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public void setParameters(Object[] parameters) {
        this.parameters = parameters;
    }
}
