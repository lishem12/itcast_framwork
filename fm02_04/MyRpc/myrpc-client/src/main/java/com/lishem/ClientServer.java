package com.lishem;

import com.lishem.core.client.NettyClient;
import com.lishem.core.client.proxy.BeanFactory;
import com.lishem.order.pojo.Item;
import com.lishem.order.pojo.Order;
import com.lishem.order.service.OrderService;

import java.util.ArrayList;
import java.util.List;

public class ClientServer {

    public static void main(String[] args) {
        //和服务端建立链接
        NettyClient nettyClient = new NettyClient();
        nettyClient.start("127.0.0.1", 5566);

        // nettyClient 有Channel属性，可以执行远程调用
        BeanFactory beanFactory = new BeanFactory(nettyClient);
        // 创建代理
        OrderService orderService = beanFactory.getBean(OrderService.class);

        List<Item> itemList = new ArrayList<>();
        Item item = new Item();
        item.setItemId(2001L);
        item.setPrice(100L);
        item.setTitle("铅笔");
        itemList.add(item);

        item = new Item();
        item.setItemId(2002L);
        item.setPrice(50L);
        item.setTitle("橡皮");
        itemList.add(item);

        for (int i = 0; i < 10; i++) {
            Order order = orderService.submitOrder(1001L, itemList);
            System.out.println("返回数据：" + order);
        }
        nettyClient.close();
    }
}
