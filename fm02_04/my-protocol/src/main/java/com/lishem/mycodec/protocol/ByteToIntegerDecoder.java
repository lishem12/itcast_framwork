package com.lishem.mycodec.protocol;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * Byte转Integer的解码器
 */
public class ByteToIntegerDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

        // int类型占4个字节，所以需要判断是否存在4个字节
        if (in.readableBytes()>=4){
            //读取到int类型数据，放入到输出，完成数据类型的转化
            out.add(in.readInt());
        }
    }
}
