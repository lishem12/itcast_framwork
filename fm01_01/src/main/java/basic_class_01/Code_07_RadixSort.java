package basic_class_01;

import static util.ArrComparator.comparator;
import static util.ArrComparator.copyArray;
import static util.ArrComparator.isEqual;

// 桶排序
public class Code_07_RadixSort {

    public static void radixSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        int maxValue = arr[0];
        for (int i = 0; i < arr.length; i++) {
            maxValue = Math.max(maxValue, arr[i]);
        }
        int digit = getNumBits(maxValue);
        int[] count = new int[10];
        int[] help = new int[arr.length];

        for (int d = 1; d <= digit; ++d) {
            for (int i = 0; i < count.length; ++i) {
                count[i] = 0;
            }
            for (int i = 0; i < arr.length; ++i) {
                count[getDigit(arr[i], d)]++;
            }
            for (int i = 1; i < 10; ++i) {
                count[i] += count[i - 1];
            }
            for (int i = arr.length - 1; i >= 0; --i) {
                help[--count[getDigit(arr[i], d)]] = arr[i];
            }
            for (int i = 0; i < arr.length; ++i) {
                arr[i] = help[i];
            }
        }
    }

    public static int getDigit(int num, int digit) {
        return (int) (num / Math.pow(10, digit - 1) % 10);
    }


    public static int getNumBits(int num) {
        int digit = 0;
        while (num != 0) {
            digit++;
            num /= 10;
        }
        return digit;
    }

    public static int[] generateRandomArray(int maxSize, int maxValue) {
        int len = (int) (Math.random() * maxSize);
        int[] arr = new int[len];
        for (int i = 0; i < arr.length; ++i) {
            arr[i] = (int) (Math.random() * maxValue);
        }
        return arr;
    }

    // for test
    public static void main(String[] args) {
        int testTime = 500000;
        int maxSize = 100;
        int maxValue = 100000;
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr1 = generateRandomArray(maxSize, maxValue);
            int[] arr2 = copyArray(arr1);
            radixSort(arr1);
            comparator(arr2);
            if (!isEqual(arr1, arr2)) {
                succeed = false;
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");
    }
}
