package basic_class_01;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import java.util.Arrays;

import static util.ArrComparator.copyArray;
import static util.ArrComparator.generateRandomArray;

public class Code_11_MaxGap {

    public static int maxGap(int[] nums) {
        if (nums == null || nums.length < 2) {
            return 0;
        }
        int maxNum = Integer.MIN_VALUE;
        int minNum = Integer.MAX_VALUE;
        for (int i = 0; i < nums.length; i++) {
            maxNum = Math.max(maxNum, nums[i]);
            minNum = Math.min(minNum, nums[i]);
        }
        if (maxNum == minNum){
            return 0;
        }
        int len = nums.length;
        int[] maxArr = new int[len+1];
        int[] minArr = new int[len+1];
        boolean[] hasNum = new boolean[len+1];
        for (int i = 0; i < nums.length; ++i) {
            // int index = (nums[i] - minNum) * len / (maxNum - minNum);
            int index = bucket(nums[i],len,minNum,maxNum);
            maxArr[index] = hasNum[index] ? Math.max(maxArr[index], nums[i]) : nums[i];
            minArr[index] = hasNum[index] ? Math.min(minArr[index], nums[i]) : nums[i];
            hasNum[index] = true;
        }
        int res = Integer.MIN_VALUE;
        int lastMax = maxArr[0];
        for (int i = 1; i < maxArr.length; ++i) {
            if (hasNum[i]) {
                res = Math.max(res, minArr[i] - lastMax);
                lastMax = maxArr[i];
            }
        }
        return res;
    }


    public static int bucket(long num, long len, long min, long max) {
        return (int) ((num - min) * len / (max - min));
    }

    public static int comparator(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        Arrays.sort(arr);
        int max = arr[1] - arr[0];
        for (int i = 1; i < arr.length; i++) {
            max = Math.max(max, arr[i] - arr[i - 1]);
        }
        return max;
    }

    // for test
    public static void main(String[] args) {
        int testTime = 500000;
        int maxSize = 100;
        int maxValue = 100;
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr1 = generateRandomArray(maxSize, maxValue);
            int[] arr2 = copyArray(arr1);
            if (maxGap(arr1) != comparator(arr2)) {
                succeed = false;
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");
    }
}
