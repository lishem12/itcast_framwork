package basic_class_01;

import java.util.Arrays;

import static util.ArrComparator.*;
import static util.ArrComparator.isEqual;

public class Code_04_QuickSort {

    public static void quickSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        quickSort(arr, 0, arr.length - 1);
    }

    public static void quickSort(int[] arr, int left, int right) {
        if (left >= right) {
            return;
        }
        int[] tmp = partation(arr, left, right);
        quickSort(arr, left, tmp[0]);
        quickSort(arr, tmp[1], right);
    }

    public static int[] partation(int[] arr, int left, int right) {
        int index = (int) (left + (right - left) * Math.random());
        swap(arr, index, right);
        int less = left - 1;
        int more = right;
        index = left;
        while (index < more) {
            if (arr[index] < arr[right]) {
                swap(arr, index++, ++less);
            } else if (arr[index] == arr[right]) {
                ++index;
            } else {
                swap(arr, index, --more);
            }
        }
        swap(arr, more++, right);
        return new int[]{less, more};
    }

    // for test
    public static void main(String[] args) {
        int testTime = 500000;
        int maxSize = 100;
        int maxValue = 100;
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr1 = generateRandomArray(maxSize, maxValue);
            int[] arr2 = copyArray(arr1);
            quickSort(arr1);
            comparator(arr2);
            if (!isEqual(arr1, arr2)) {
                succeed = false;
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");
    }
}
