package basic_class_01;

import java.util.Arrays;

import static util.ArrComparator.*;
import static util.ArrComparator.isEqual;

public class Code_03_HeapSort {

    public static void heapSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        for (int i = 0; i < arr.length; ++i) {
            heapInsert(arr, i);
        }
        for (int i = arr.length - 1; i >= 0; --i) {
            swap(arr, i, 0);
            heapIfy(arr, 0, i);
        }
    }

    private static void heapIfy(int[] arr, int index, int size) {
        int left = index * 2 + 1;
        while (left < size) {
            int maxIndex = arr[index] > arr[left] ? index : left;
            maxIndex = left + 1 < size ? (arr[left + 1] > arr[maxIndex] ? left + 1 : maxIndex) : maxIndex;
            if (maxIndex == index) {
                break;
            }
            swap(arr, maxIndex, index);
            index = maxIndex;
            left = index * 2 + 1;
        }
    }

    public static void heapInsert(int[] arr, int size) {
        while (arr[size] > arr[(size - 1) / 2]) {
            swap(arr, size, (size - 1) / 2);
            size = (size - 1) / 2;
        }
    }

    // for test
    public static void main(String[] args) {
        int testTime = 500000;
        int maxSize = 100;
        int maxValue = 100;
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr1 = generateRandomArray(maxSize, maxValue);
            int[] arr2 = copyArray(arr1);
            heapSort(arr1);
            comparator(arr2);
            if (!isEqual(arr1, arr2)) {
                System.out.println(Arrays.toString(arr1));
                System.out.println(Arrays.toString(arr2));
                succeed = false;
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");
    }

}
