package basic_class_01;

import static util.ArrComparator.comparator;
import static util.ArrComparator.copyArray;
import static util.ArrComparator.isEqual;

// 计数排序
public class Code_06_BucketSort {

    public static void bucketSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        int maxValue = arr[0];
        for (int i = 0; i < arr.length; ++i) {
            maxValue = Math.max(maxValue, arr[i]);
        }
        int[] help = new int[maxValue + 1];
        for (int i = 0; i < arr.length; ++i) {
            help[arr[i]]++;
        }
        int index = 0;
        for(int i = 0;i<help.length;++i){
            while (help[i] != 0){
                arr[index++] = i;
                --help[i];
            }
        }
    }

    public static int[] generateRandomArray(int maxSize, int maxValue) {
        int len = (int) (Math.random() * maxSize);
        int[] arr = new int[len];
        for (int i = 0; i < arr.length; ++i) {
            arr[i] = (int) (Math.random() * maxValue);
        }
        return arr;
    }

    // for test
    public static void main(String[] args) {
        int testTime = 500000;
        int maxSize = 100;
        int maxValue = 150;
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr1 = generateRandomArray(maxSize, maxValue);
            int[] arr2 = copyArray(arr1);
            bucketSort(arr1);
            comparator(arr2);
            if (!isEqual(arr1, arr2)) {
                succeed = false;
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");


    }


}
