package basic_class_01;

import util.ArrComparator;

public class Code_00_BubbleSort {

    public static void bubbleSort(int[] nums) {
        if (null == nums || nums.length < 2) {
            return;
        }
        for (int i = nums.length - 1; i > 0; --i) {
            for (int j = 0; j < i; j++) {
                if (nums[j] > nums[j + 1]) {
                    ArrComparator.swap(nums, j, j + 1);
                }
            }
        }
    }

    // for test
    public static void main(String[] args) {
        int testTime = 500000;
        int maxSize = 100;
        int maxValue = 100;
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr1 = ArrComparator.generateRandomArray(maxSize, maxValue);
            int[] arr2 = ArrComparator.copyArray(arr1);
            bubbleSort(arr1);
            ArrComparator.comparator(arr2);
            if (!ArrComparator.isEqual(arr1, arr2)) {
                succeed = false;
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");
    }
}
