package basic_class_01;

import java.util.Arrays;

import static util.ArrComparator.swap;

public class Code_08_NetherlandsFlag {

    public static int[] partition(int[] arr, int l, int r, int p) {
        int less = l - 1;
        int more = r + 1;
        int index = l;
        while (index < more) {
            if (arr[index] < p) {
                swap(arr,index++,++less);
            } else if (arr[index] == p) {
                ++index;
            } else {
                swap(arr,index,--more);
            }
        }
        return new int[]{less,more};
    }


    // for test
    public static int[] generateArray() {
        int[] arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 3);
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] test = generateArray();

        System.out.println(Arrays.toString(test));
        int[] res = partition(test, 0, test.length - 1, 1);
        System.out.println(Arrays.toString(test));
        System.out.println(res[0]);
        System.out.println(res[1]);

    }


}
